<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Feels Like</title>

		<link rel="stylesheet" href="css/main.css" type="text/css" />

		<!--[if IE]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<!--[if lte IE 7]>
			<script src="js/IE8.js" type="text/javascript"></script><![endif]-->
		<!--[if lt IE 7]>

			<link rel="stylesheet" type="text/css" media="all" href="css/ie6.css"/><![endif]-->
	
	</head>

	<body>
		<section id='weather'>
			<?
				include 'connect.php';
				$date = getdate	(time());
				$year = $date['year'];
				$todaysDate = $date['yday'];
				$yesterdaysDate = $date['yday'] - 1;
				$zip = "94102";
				// echo('today:' . $todaysDate . '<br>');
				// echo('yesterdaysDate:' . $yesterdaysDate . '<br>');

				$query = "SELECT * FROM temps WHERE zip = 94102 AND year = 2011 AND day BETWEEN " . $yesterdaysDate . " AND " . $todaysDate . " LIMIT 0, 2";
				$result = mysql_query($query);
				$years;
				$arr;
				while($row = mysql_fetch_array($result))
				{
					//$years[$row['year']] = $row['year'];
					$newYear = $row['year'];
					if(is_null($years[$newYear]))
					{
						$arr = array($row);
					} else {
						$arr[] = $row;
					}
					$years[$row['year']] = $arr;
				}
				$today;
				$yesterday;

				foreach($years as $year)
				{
					foreach($year as $entry)
					{
						if($entry['day'] == $todaysDate)
						{
							$today = $entry;	
							// echo($today);
						} else if($entry['day'] == $yesterdaysDate) {
							$yesterday = $entry;	
							// echo($yesterday);
						}
					}
				}
				$todaysTemp = $today['f'];
				$yesterdaysTemp = $yesterday['f'];
				$tempDelta = $todaysTemp - $yesterdaysTemp;
				$relativeTimeString = "yesterday";
				$tempString = "Today feels ";
				 // echo('todaysTemp: ' . $todaysTemp . '<br>');
				 // 	 echo('yesterdaysTemp: ' . $yesterdaysTemp . '<br>');
				 // 	 echo('tempDelta: ' . $tempDelta . '<br>');

				if($tempDelta == 0)
				{
					$tempString = $tempString . "just like ";
				} else if($tempDelta < 0){
						$tempString = $tempString . deltaCheck(abs($tempDelta), 'cooler');
				} else if($tempDelta > 0) {
					$tempString = $tempString . deltaCheck(abs($tempDelta), 'warmer');
				}

				$tempString = $tempString . $relativeTimeString;

			//	echo($tempString);

				function deltaCheck($delta, $relationship)
				{
					return  $delta . " degrees " . $relationship . " than ";

					// if($delta <= 4)
					// 		return "a little " . $relationship . " than ";
					// 	else if($delta <= 15)
					// 		return  "a lot" . $relationship . " than ";
					// 	else if($delta <= 30)
					// 		return "sooo much  " . $relationship . "  than ";
				}

				include 'close.php';
			?>
			<p><? print($tempString) ?></p>
		</section>
	</body>
</html>